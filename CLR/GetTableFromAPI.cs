using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;
using System.Net;
using System.IO;
using Newtonsoft.Json;

public partial class StoredProcedures
{
    [Microsoft.SqlServer.Server.SqlProcedure]
    public static void GetTableFromAPI(SqlString UserName, SqlString Password)
    {
       
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(string.Format("http://getsampledata.webpixma.com/api/values?username={0}&password={1}", UserName, Password));

        request.Method = "GET";
        request.ContentLength = 0;
        request.Credentials = CredentialCache.DefaultCredentials;
        HttpWebResponse response = (HttpWebResponse)request.GetResponse();

        Stream stream = response.GetResponseStream();
        StreamReader sr = new StreamReader(stream);
        string jsonText = sr.ReadToEnd();



        JsonSerializer json = new JsonSerializer();

        StringReader strR = new StringReader(jsonText);
        JsonTextReader reader = new JsonTextReader(strR);

        DataTable dt = (DataTable)json.Deserialize(reader, typeof(DataTable));

        reader.Close();


        
        SendDataTable(dt);
    }

    public static void SendDataTable(DataTable dt)
    {
        SqlMetaData[] metaData = new SqlMetaData[] { new SqlMetaData("Number", SqlDbType.BigInt), new SqlMetaData("Name", SqlDbType.VarChar, 8000) };

        SqlDataRecord record = new SqlDataRecord(metaData);
        SqlPipe pipe = SqlContext.Pipe;
        pipe.SendResultsStart(record);
        try
        {
            if (dt != null)
                foreach (DataRow row in dt.Rows)
                {
                    for (int index = 0; index < record.FieldCount; index++)
                    {
                        object value = row[index];
                        record.SetValue(index, value);
                    }

                    pipe.SendResultsRow(record);
                }
        }
        finally
        {
            pipe.SendResultsEnd();
        }
    }
}
