USE [master]
GO



--Crate a new database.
--Set the location were you want to store the database.
CREATE DATABASE [CLRTest] ON  PRIMARY 
( NAME = N'CLRTest', FILENAME = N'c:\temp\CLRTest.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'CLRTest_log', FILENAME = N'c:\temp\CLRTest_log.ldf' , SIZE = 1792KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO


--Update database settings to allow the assembly to run inside sql server.
alter database [CLRTest] set trustworthy on;  

GO

use [CLRTest]
Go



sp_configure 'show advanced options', 1;  
GO  
RECONFIGURE;  
GO  
sp_configure 'clr enabled', 1;  
GO  
RECONFIGURE;  
GO  

if(@@VERSION like 'Microsoft SQL Server 2008%')  
begin

--Add Serialization assembly reference which is required by Newton.JSON assembly. Just make sure System.Runtime.Serialization.dll file is located inside below specified location.
CREATE ASSEMBLY System_Runtime_Serialization FROM 'C:\Windows\Microsoft.NET\Framework\v3.0\Windows Communication Foundation\System.Runtime.Serialization.dll'
WITH PERMISSION_SET = UNSAFE

end
else
begin

--Add Serialization assembly reference which is required by Newton.JSON assembly. Just make sure System.Runtime.Serialization.dll file is located inside below specified location.
CREATE ASSEMBLY System_Runtime_Serialization FROM 'C:\Windows\Microsoft.NET\Framework\v4.0.30319\System.Runtime.Serialization.dll'
WITH PERMISSION_SET = UNSAFE

end

GO


-- Set your application path were the CLR.dll is located with leadind '\'. For example ...\CLR\bin\Debug\
declare @applicationPath as varchar(max)
set @applicationPath='C:\Dropbox\Work\FE\Clients\Upwork SQL CLR Demo project\SqlToWebService\CLR\bin\Debug\'



--Add Newton.JSON assembly assembly reference.
CREATE ASSEMBLY [Newtonsoft.Json] AUTHORIZATION [dbo]
FROM @applicationPath+'Newtonsoft.Json.dll'
WITH PERMISSION_SET = UNSAFE


--Add CLR (our custom code) assembly refernce.
CREATE ASSEMBLY [CLR] AUTHORIZATION [dbo]
FROM @applicationPath+'CLR.dll'
WITH PERMISSION_SET = UNSAFE



Go

-- Create a store procedure as external and point it to our custom code assembly.
CREATE PROCEDURE [dbo].[GetTableFromAPI] @UserName [nvarchar](MAX), @Password [nvarchar](MAX)
AS EXTERNAL NAME [CLR].[StoredProcedures].[GetTableFromAPI];

GO


--Finally execute the assembly to check the result.
exec [dbo].[GetTableFromAPI] 'avtar','mori'