1) Build the CLR project. I am using live hosted version of api http://getsampledata.webpixma.com/api/values?username=avtar&password=mori inside CLR project for testing purpose. It is same as the one included inside the solution.

2) Open the RegisterAssembly.sql file inside your sql server. Please read the comments inside this file before executing it because you need to set two paths. One is location of your CLR Projects debug folder were the assemblies are located and another path you need to set is were you want to store your new CLRTest database.

3) If you find any issue please forward it to avtar.mori@gmail.com with screenshoot.